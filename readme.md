# How to use PCL modules in your projects

*You cannot use PCL modules in localhost

For using PCL modules you need to create a file or you can clone the file from https://bitbucket.org/produktiv/pcl-config-files/src 

# Instagram Feed Module PHP
        require_once '../../escobar/ig-feed/ProduktivInstagramFeed.php';
        if($_POST['module'] === 'instagram_feed'){
            $instaFeed = new ProduktivInstagramFeed(
                array("auth_token"=>'YOUR_TOKEN_HERE', "limit"=> 60)
            );
            echo $instaFeed->getInstagramFeed();
        } 

# Instagram Feed Module JS
    jQuery.post("produktiv-modules.php", { module: 'instagram_feed' }, function(data, status){   
        var instaData = JSON.parse(data);                 
        if(instaData.data){
            instaData.data.forEach(insta => {
            console.log(insta);             
            $("#instaFeed").append('<img src="'+ insta.images.standard_resolution.url +'"/>');
        });
         }
    });

# Twitter Feed Module PHP
    require_once '../../escobar/twitter-feed/ProduktivTwitterFeed.php'; // Twitter Feed Module Import
    if($_POST['module'] === 'twitter_feed'){
        $tweet = new ProduktivTwitterFeed(
            array(
                'oauth_access_token' => "YOUR_TOKEN_HERE",
                'oauth_access_token_secret' => "YOUR_TOKEN_HERE",
                'consumer_key' => "YOUR_TOKEN_HERE",
                'consumer_secret' => "YOUR_TOKEN_HERE"
            )
        );
        
        echo $tweet->fetchTwitterUserFeed(); 
    }
# Twitter Feed Module JS
    jQuery.post("produktiv-modules.php", { module: 'twitter_feed' }, function(data, status){   
        var twitterData = JSON.parse(data);                 
        if(twitterData){
            twitterData.forEach(twitter => {
              $("#twitterFeed").append(`<div class="twitter-card">
                  <h2>${twitter.user.name} <br/> <small>${twitter.created_at}</small></h2>                  
                  <p>${twitter.text}</p>
                </div>`);
            });            
        }
    });

# Email Form Module PHP 
    require_once '../../escobar/email-form/ProduktvEmailForm.php'; // Email Form Module Import
    if($_POST['module'] === 'contact_form'){
        $sendEmail = new ProduktvEmailForm(
            array( 
                "smtp_host"=>'HOST_HERE', 
                "smtp_username"=> "USERNAME_HERE",
                "smtp_pw"=> "PASSWORD",
                "from_email" => $_POST['cEmail'],
                "from_name" => "FROM_NAME",
                "to_email" => "TO_EMAIL",
                "to_name" => "TO_NAME",
                "email_subject" => $_POST['cSubject'],
                "email_body" => $_POST['cMsg']
            )
        );
            
        $sendEmail->sendEmail();
    }

# Email Form Module HTML/JS 
    <form id="sampleForm" action="POST" action="produktiv-modules.php">
      <div class="form-group">
        <label for="exampleInputEmail1">Email address</label>
        <input type="email" class="form-control" name="cEmail">
        <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
      </div>
      <div class="form-group">
        <label for="exampleInputPassword1">Subject</label>
        <input type="text" class="form-control" name="cSubject">
      </div>
      <div class="form-group">
        <label for="exampleInputPassword1">Message</label>
        <textarea class="form-control" name="cMsg"></textarea>
      </div>     
      <button type="submit" class="btn btn-primary">Submit</button>
    </form>
    
    jQuery( "#sampleForm" ).on( "submit", function( event ) {
        event.preventDefault();
        jQuery.post("produktiv-modules.php",  jQuery( this ).serialize()+'&module=contact_form' , function(data, status){   
            console.log(data); // Do the front end work with data         
        });
    });
