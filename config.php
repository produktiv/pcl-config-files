<?php    
    require_once '../../escobar/ig-feed/ProduktivInstagramFeed.php'; //Instagram Feed Module Import
    require_once '../../escobar/twitter-feed/ProduktivTwitterFeed.php'; // Twitter Feed Module Import
    require_once '../../escobar/email-form/ProduktvEmailForm.php'; // Email Form Module Import
   

    if($_POST['module'] === 'instagram_feed'){
        $instaFeed = new ProduktivInstagramFeed(
            array("auth_token"=>'YOUR_TOKEN_HERE', "limit"=> 60)
        );
            
        echo $instaFeed->getInstagramFeed();
    }   

    if($_POST['module'] === 'twitter_feed'){
        $tweet = new ProduktivTwitterFeed(
            array(
                'oauth_access_token' => "YOUR_TOKEN_HERE",
                'oauth_access_token_secret' => "YOUR_TOKEN_HERE",
                'consumer_key' => "YOUR_TOKEN_HERE",
                'consumer_secret' => "YOUR_TOKEN_HERE"
            )
        );
        
        echo $tweet->fetchTwitterUserFeed(); 
    }

    if($_POST['module'] === 'contact_form'){
        $sendEmail = new ProduktvEmailForm(
            array( 
                "smtp_host"=>'HOST_HERE', 
                "smtp_username"=> "USERNAME_HERE",
                "smtp_pw"=> "PASSWORD",
                "from_email" => $_POST['cEmail'],
                "from_name" => "FROM_NAME",
                "to_email" => "TO_EMAIL",
                "to_name" => "TO_NAME",
                "email_subject" => $_POST['cSubject'],
                "email_body" => $_POST['cMsg']
            )
        );
            
        $sendEmail->sendEmail();
    }
?>